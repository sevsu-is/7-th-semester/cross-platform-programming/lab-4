import QtQuick 2.12
import QtQuick.Layouts 1.3

Rectangle {
    height: 100
    width: 200
    radius: 6

    border.color: "#3498db"
    Layout.fillWidth: true

    property string defaultText: ""
    signal compareTextChanged()

    function getText() {
        return textEditField.text
    }

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
    }

    TextEdit {
        id: textEditField
        anchors.fill: parent
        anchors.margins: 5
        color: "#3498db"
        onTextChanged: compareTextChanged()
        text: defaultText
    }
}
