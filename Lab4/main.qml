import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    width: 500
    height: 300
    visible: true
    title: qsTr("Text comparator")

    function textEditChanged() {
        compareLabel.reset()
    }

    function doComparison() {
        if (textEdit1.getText() === textEdit2.getText()) {
            compareLabel.matches()
        } else {
            compareLabel.doesNotMatch()
        }
    }

    ColumnLayout {
        spacing: 5
        anchors.fill: parent
        anchors.margins: 10

        CompareTextEdit {
            id: textEdit1
            defaultText: "Lorem ipsum"
            onCompareTextChanged: textEditChanged()
        }

        CompareTextEdit {
            id: textEdit2
            defaultText: "Lorem ipsum"
            onCompareTextChanged: textEditChanged()
        }

        RowLayout {
            spacing: 10
            CompareButton {
                label: "Compare"
                onButtonClicked: doComparison()
            }
            CompareLabel {
                id: compareLabel
            }
        }
    }
}
