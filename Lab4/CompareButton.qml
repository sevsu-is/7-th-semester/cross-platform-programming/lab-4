import QtQuick 2.12

Rectangle {
    height: 50
    width: 100
    radius: 6
    color: "#3498db"
    scale: buttonMouseArea.pressed ? 1.1 : 1.00

    property string label: "Button"
    signal buttonClicked()

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
        onClicked: buttonClicked()
    }

    Text {
        color: "#ffffff"
        text: label
        anchors.centerIn: parent
    }
}
